# gort
[![Go Report Card](https://goreportcard.com/badge/github.com/Erdk/gort)](https://goreportcard.com/report/github.com/Erdk/gort)

Simple ray tracer written in Go.

![alt tag](https://raw.githubusercontent.com/Erdk/gort/master/static/output.png)

## Instalation

```
go get github.com/Erdk/gort
cd gort
glide up
go build .
./gort -h
```

## Rationale

Gort started as my "pet project" I'm working on in my spare time, 
and I belive it'll stay this way for now, so please don't expect 
regular updates ;) The idea came to my  mind after seeing Peter 
Shirley's books on Amazon about ray-tracing: "Ray Tracing in One Weekend", 
"Ray Tracing: The Next Week" and 
 "Ray Tracing: The Rest of Your Life". I thought that this would be a 
 good way to improve my programming skills and learn more about Golang
 ecosystem and best practises. Soo.. here we are :) As for books I 
 highly recommend them, they're well written and quite easy to understand.

## Options

```
    -w <width>
        Width of generated image, by default 640.
    -h <height>
        Height of generated image, by default 480.
    -s <samples-per-pixel>
        Number of rays per pixel, by default 400.
    -t <threads>
        Number of parallel rendering jobs, by default 1. Set 0 for auto (max CPU threads).
    -o <output-file>
        Filename (without extension) of png with output, by default "output".
    -i <input-file>
        Filename of input file, if this flag is used then scene won't be randomly generated. By default empty.
    -j <save-scene>
        Save genereated scene before render to <output>.json, by default false.
    -prof <profile>
        Generate profiling profile to use with 'go tool pprof'. By default none (no profiling). Available profiles:
        - cpu 
        - mem
        - block
    -p
        If present it'll show progress (in %).
    -cu AxB
        Compute unit, number of patch to compute at once by goroutine, by default 16x16.
```
